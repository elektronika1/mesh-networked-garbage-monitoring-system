# 802.11 b/g/n based Mesh Networked Garbage Monitoring System

Garbage monitoring is very prominent amongst the hobbyist community. This project tries to scale up the solution with mesh networking provisioned by ESP32's '**Combined AP-STA mode**' to interlink the garbage monitoring equipment. 

<img src="./readme_support/unmonitored_Garbage.jpg" alt="Unmonitored garbage"  width="512" height="288" style="vertical-align:middle" />

Garbage Monitoring equipment was designed using  Loadcell and an ultrasonic sensor that measures, the weight of the bin and level of the bin respectively.  The picture below depicts the garbage monitoring equipment. A custom option to trigger the alert is provided using a button and is depicted in the architecture. 

<img src="./readme_support/GaMon.jpg" alt="Garbage Monitoring Equipment " width="450" height="500" style="zoom:33%;" />

The garbage equipment thus designed is connected to a router as shown in below architecture. This architecture works great where single garbage monitoring is performed.  

<img src="./readme_support/block_diagram.png" alt="Architecture of Single Node" width="480" height="270" style="zoom: 67%;" />

Think of large complex environments like public transport stations, gated communities, where multiple bins (let's say roughly 10+)  needs to be monitored by connecting to a Wi-Fi Access Point (AP) isn't feasible due to the wastage of precious AP and it's bandwidth. 

For these environments a networked garbage monitoring is preferred, where a single node will connect to  Wi-Fi AP and rest of the nodes are interconnected in P2P Mesh whose architecture is shown below.

<img src="./readme_support/Mesh_Node_Garbage_Bin.png" alt="Mesh Networked Garbage monitoring " width="480" height="480" style="zoom:60%;" />

This mesh network architecture is possible using existing technologies like Bluetooth, ZigBee etc.. which results in multiple communication protocols and RF noise. To avoid that, **this project uses WiFi (802.11 b/g/n) protocol itself to create the mesh network**. This is possible by ESP32's 'combined AP-STA ode', where a single ESP32 can be **Sta**tion as well as **A**ccess **P**oint that results in creating more self-healing nodes and the ability to extend the network depends on the ESP32 constraints. Currently each device can accommodate 7 devices as STA with deeper stacking as large as 15.  

The node that connects to the Actual Wi-Fi that has Internet connectivity is called Master node and rest of them are slave nodes. 
Internal Load cell and Ultrasonic sensor calibration is provided along with the sketch. 

Libraries used:

- **painlessMesh** for Wi-Fi mesh networking

- **AsyncWebserver** for Serving static webpages on Local Connection

- **IotTweet** for logging data every 10seconds on iottweet.com website

- **IFTTTWebhook** for triggering SMS/Notification Alerts

  

## Working of Mesh networked Garbage Monitoring System

The master node can be discovered in local area network using zero configuration networking (zeroconf) by utilizing the mDNS service records with a call to 'garbagenode.local' whose response will be a static served webpage containing the details of garbage nodes that reported to the master node.

<img src="./readme_support/Webpage_Served.jpg" alt="Static webpage"  width="640" height="360"  style="zoom: 50%;" />

Remote monitoring is made possible using iottweet.com where each bin time line is available, based on the updates provided to the remote.  The depicts individual bin and its state of monitoring with time stamps.

<img src="./readme_support/Trash_on_emptying.jpg" alt="Remote Monitoring"  width="640" height="360"  style="zoom:50%;" />

When bin is filled or a trigger is invoked by any of the nodes those will be alerted using IFTTT Webhooks service. 

<img src="./readme_support/IFTTT_Activity.png" alt="IFTTT activity" width="270" height="480"  style="zoom:33%;" />



## Author

* **Babu P S** - [eflaner](https://gitlab.com/eflaner)
* **Rabina Jose**
* **Mumthas A J**
* **Keerthana Gopi**

## License

This project uses Hardware licensed under the [CERN Open Hardware Licence v2](http://www.opensource.org/licenses/mit-license.php) and the firmware with [MIT License](http://www.opensource.org/licenses/mit-license.php).

## Acknowledgements

This project uses various libraries from the esp32 to acheive the objective. 
Indebted to people who provided suggestions for improvement and made this possible.
