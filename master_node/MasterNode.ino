/*
  Sketch for Garbage Monitoring System
  Developer: Babu P S, Mumthas A J, Rabina Jose, Keerthana Gopi
  Developed for: DOE, CUSAT as part of ES Project
  Libraries Used: AsyncWebserver for Serving static webpages on Local Connection
                  index.h for storing HTML,CSS and Javascript of Webpage
                  painlessMesh for Mesh networking
                  IotTweet for logging data every 10seconds on iottweet.com website
                  IFTTTWebhook for triggering SMS/Notification Alerts
  Sensors Interfaced: Weight sensor interfaced through HX711 ADC
                      Ultrasonic Sensor Interfaced Directly

  See LICENSE badge for licensing info
*/
//==============================================================
//                 Libraries used
//==============================================================
//#include <esp_int_wdt.h>
//#include <esp_task_wdt.h>
#include <WiFi.h>
#include <HX711_ADC.h>
#include "index.h" // HTML webpage contents with javascripts
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <IoTtweetESP32.h>
#include "IFTTTWebhook.h"
#include "painlessMesh.h"

//==============================================================
//                 CONSTANTS used
//==============================================================
#define LED 2         //On board LED
#define TOUCH_PIN T7  // D27 CAPACITIVE TOUCH PIN
#define BUTTONPIN 0   // the pin that the pushbutton is attached to
#define DOUT 4        // HX711 Data out
#define CLK  15       // HX711 CLK input
#define THRESH 40     // Touch Capp Threshold
#define RANGE_MIN 35  // Trash bin Minimum value (As distance reduces garbage fills)
#define RANGE_MAX 30
#define TRIGPIN 32   //D32 // defines Ultrasonic pin numbers
#define ECHOPIN 33   //D33
#define EXT_LED 23   //D23 // CAPACITIVE TOUCH PIN FEEDBACK LED

//SSID and Password WiFi router - Hardcoded
#define WIFI_SSID "xxxxxxxxx"
#define WIFI_PASSWORD "xxxxxxxx"
//IFTTT API KEY and EVENT
#define IFTTT_API_KEY "Xxxxxxxxxxxxxxxxxxxxxxxxxx"
#define IFTTT_EVENT_NAME "Trash_Filled"
//MESH NETWORK CONNECTION CREDENTIALS
#define MESH_PREFIX     "GarbageBinNetwork"
#define MESH_PASSWORD   "TrashedGarbageWaste"
#define MESH_PORT       5555
//Miscellaneous Constants
#define HOST_NAME       "TrashBinNode"
#define NODE_NAME       "Laboratory1"
#define PACKET_TYPE_0 0
#define PACKET_TYPE_1 1
#define NUM_OF_NODES 2 // Hardcoded number of nodes - Space constraints
#define WEIGHT_THRESH 250
#define LEVEL_THRESH 90
#define COUNT_THRESHH 20
//IoTTweet credentials
const char *userid = "xxxxx"; //IoTtweet account user ID (6 digits, included zero pre-fix)
const char *key = "xxxxxxxxx"; //IoTtweet registered device key in "MY IOT Garage"
//==============================================================
//                 Global Variables used
//==============================================================
uint64_t chipid[NUM_OF_NODES] = {0}; //Global Storage of CHipIDs
uint8_t   station_ip[4] =  {192, 168, 43, 65}; // IP we need - Hazard ahead for clash
long t; // Time count for Hx711 ADC
float weight[NUM_OF_NODES];
float level[NUM_OF_NODES];
bool sendAlarm = false;
bool BinFull = false;
hw_timer_t * timer = NULL; // Timer to keep track of Touch switch pressed
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;
volatile uint8_t isrCounter = 0; // counts seconds passed after pressing switch
volatile uint16_t newCounter=0;//Counts the BinFull parameter calculation
long duration; // used for the Ultrasonic Sensor
//variables used to set Tare to Zero right from ESP32 - Disabled after boxing
boolean buttonState = 0;         // current state of the button
boolean lastButtonState = 0;     // previous state of the button
String private_tweet = NODE_NAME;     // Your private tweet meassage to dashboard
String public_tweet = "Garbage Monitoring"; //Your public tweet message to dashboard
//==============================================================
//                 INSTANTIATIONS
//==============================================================
HX711_ADC LoadCell(DOUT, CLK);
IoTtweetESP32 myiot;
painlessMesh mesh;
Scheduler userScheduler;
AsyncWebServer server(80); //Server on port 80
//===============================================================
// This routine to handle touch and timer interrupt
//===============================================================
void gotTouch1() {
  digitalWrite(EXT_LED, HIGH);
  timerAlarmEnable(timer);
}
void IRAM_ATTR onTimer() {
  portENTER_CRITICAL_ISR(&timerMux);
  if (touchRead(TOUCH_PIN) < THRESH) {
    isrCounter++; // Increment the counter
  }
  else {
    if ((isrCounter > 0) && (isrCounter < 10))
      sendAlarm = true;
    isrCounter = 0;
    digitalWrite(EXT_LED, LOW);
    timerAlarmDisable(timer);
  }
  portEXIT_CRITICAL_ISR(&timerMux);
}
//===============================================================
// Routine to update the values of the bin to other nodes
//===============================================================
Task logClientTask(11000, TASK_FOREVER, []() {
  String strJs = provideJson(-1, weight[0], level[0], chipid[0], PACKET_TYPE_0);
  mesh.sendBroadcast(strJs);
  Serial.printf("\n");
  if (WiFi.status() == WL_CONNECTED) {
    myiot.WriteDashboard(userid, key, (weight[0] / 10.0), level[0], 0.0, 0.0, private_tweet, public_tweet);
    Serial.println("Sent tweet");
  }
});
//===============================================================
// Routine to convert packets into JSON format for webpage updation and Node updation
// packetType - 0 - Broadcast selfvalues
// packetType - 1 - Broadcast Trigger of this node
//===============================================================
String provideJson(int i, float Weight, float LeveL, uint64_t chip_id, uint8_t packettype) {
  String strJson;
  strJson += "{\"Packet\": ";
  strJson += packettype;
  strJson += ",\"LoadCell" + String(i + 1);
  strJson += "\": ";
  strJson += Weight / 10;
  strJson += ",\"UltrasonicSensor" + String(i + 1);
  strJson += "\": ";
  strJson += LeveL;
  strJson += ",\"chipsetID" + String(i + 1);
  strJson += "\": ";
  strJson += (uint16_t)(chip_id >> 32);
  strJson += (uint32_t)chip_id;
  if (packettype == 1) {
    strJson += ",\"IFTTTKey\": ";
    strJson += IFTTT_API_KEY;
    strJson += ",\"BinName\": ";
    strJson += NODE_NAME;
  }
  strJson += "\}";
  return strJson;
}
//===============================================================
// This routine is executed when browser asks for wrong field
//===============================================================
void onRequest(AsyncWebServerRequest *request) {
  AsyncWebServerResponse *response = request->beginResponse(404); //Sends 404 File Not Found
  response->addHeader("Server", "Garbage Monitoring Device");
  request->send(response);   //Handle Unknown Request
}
//==============================================================
//                  SETUP
//==============================================================
void setup(void) {
  Serial.begin(115200);
  Serial.print("Wait for reset pin to release.");
  while (touchRead(TOUCH_PIN) < THRESH)
    Serial.print("."); // Wait for reset pin to release
  Serial.println("");
  //Onboard LED port Direction output
  pinMode(EXT_LED, OUTPUT); // Sets the EXT_LED pin as an Output
  pinMode(TRIGPIN, OUTPUT); // Sets the TRIGPIN as an Output
  pinMode(ECHOPIN, INPUT);  // Sets the ECHOPIN as an Input
  pinMode(BUTTONPIN, INPUT);// initialize the button pin as a input
  pinMode(LED, OUTPUT); // Set onboard led as output
  digitalWrite(EXT_LED, LOW);// Sets Touch Feedback led as low
  LoadCell.begin(); //Start HX711
  for (uint8_t t = 4; t > 0; t--) { // Warmup for HX711
    Serial.printf(".");
    Serial.flush();
    delay(1000);
  }
  uint16_t stabilisingtime = 3000; // tare preciscion can be improved by adding a few seconds of stabilising time
  LoadCell.start(stabilisingtime);
  LoadCell.setCalFactor(-44.0); // user set calibration factor (float)
  Serial.println("Startup + tare is complete");
  //  WiFi.mode(WIFI_AP_STA);
  // Initialize MESH Network
  mesh.init( MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT, WIFI_AP_STA, 4,1);
  Serial.println("My AP IP is " + mesh.getAPIP().toString());
  //  mesh.stationManual(WIFI_SSID, WIFI_PASSWORD, MESH_PORT, station_ip);
  mesh.stationManual(WIFI_SSID, WIFI_PASSWORD);//Optionally Provide PORT and IP
  mesh.setHostname(HOST_NAME);
  //  mesh.setRoot(true);
  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection([](size_t nodeId) {
    Serial.printf("New Connection %u\n", nodeId);
  });
  mesh.onDroppedConnection([](size_t nodeId) {
    Serial.printf("Dropped Connection %u\n", nodeId);
  });
  Serial.println("");
  // Wait for connection
  //  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);     //Connect to your WiFi router
  //  while (WiFi.status() != WL_CONNECTED) {
  //    delay(500);
  //    Serial.print(".");
  //  }
  Serial.println("My IP is " + mesh.getStationIP().toString());

  chipid[0] = ESP.getEfuseMac(); //The chip ID is essentially its MAC address(length: 6 bytes).
  Serial.printf("ESP32 Chip ID = %04X", (uint16_t)(chipid[0] >> 32)); //print High 2 bytes
  Serial.printf("%08X\n", (uint32_t)chipid[0]); //print Low 4bytes.
  //If connection successful show IP address in serial monitor
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(WIFI_SSID);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());  //IP address assigned to your ESP
  Serial.println("Wait...");
  // Serve webpage on client Connection
  server.on("/", HTTP_ANY, [](AsyncWebServerRequest * request) {
    String s = MAIN_page; //Read HTML contents
    AsyncWebServerResponse *response = request->beginResponse(200, "text/html", s);
    response->addHeader("Server", "Garbage Monitoring Device");
    request->send(response);
  });
  // Respond to the AJAX request on readsensors
  server.on("/readSensors", HTTP_GET, [](AsyncWebServerRequest * request) {
    String strJson = "[";
    for (int i = 0; i < NUM_OF_NODES; ++i) {
      if (chipid[i] != 0) {
        if (i) strJson += ",";
                strJson += "{\"LoadCell"+String(i+1);
                strJson += "\": ";
                strJson += weight[i] / 10;
                strJson += ",\"UltrasonicSensor"+String(i+1);
                strJson += "\": ";
                strJson += level[i];
                strJson += ",\"chipsetID"+String(i+1);
                strJson += "\": ";
                strJson += (uint16_t)(chipid[i] >> 32);
                strJson += (uint32_t)chipid[i];
                strJson += "\}";
//        strJson += provideJson(i, weight[i], level[i], chipid[i], PACKET_TYPE_0);
      }
    }
    strJson += "]";
    request->send(200, "application/json", strJson);
  });
  server.onNotFound(onRequest);
  //start interrupt to detect touch pin
  touchAttachInterrupt(TOUCH_PIN, gotTouch1, THRESH);
  timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, &onTimer, true);
  timerAlarmWrite(timer, 1000000, true);
  server.begin(); //Start server
  Serial.println("HTTP server started");
  userScheduler.addTask(logClientTask);
  logClientTask.enable();//Run the node updation schema
}
//==============================================================
//                     LOOP
//==============================================================
void loop(void) {
  userScheduler.execute();
  mesh.update();
  if (sendAlarm) {
    Serial.print("Alert code is: ");
    TriggerAlerts(IFTTT_API_KEY, (weight[0] / 10.0), level[0], NODE_NAME, true);
    sendAlarm = false;
    digitalWrite(EXT_LED, LOW);
  }
  else if (isrCounter > 10) { //RESET IS INITIATED USING TOUCH BUTTON
    isrCounter = 0;
    timerAlarmDisable(timer);
    mesh.stop();
    userScheduler.disableAll();
    digitalWrite(EXT_LED, LOW);
    delay(500);
    digitalWrite(EXT_LED, HIGH);
    delay(500);
    digitalWrite(EXT_LED, LOW);
    delay(500);
    digitalWrite(EXT_LED, HIGH);
    delay(500);
    digitalWrite(EXT_LED, LOW);
    delay(500);
    digitalWrite(EXT_LED, HIGH);
    Serial.println("Reset ESP32");
    ESP.restart();
    //      hard_restart();
  }
  LoadCell.update(); //Read the current Weight sensed by Loadcell
  digitalWrite(LED, LOW);
  //  int touch_value = touchRead(TOUCH_PIN);
  if (millis() > t + 250) {           // Get smoothed value from data set + current calibration factor
    digitalWrite(LED, HIGH);
    interfaceUSonic(); //Get the ultrasonic sensor value
    float i = fabs(LoadCell.getData()); //avoid negative readings of weight- it has no significance
    Serial.print("     Weight sensed (gm) : ");
    weight[0] = i;
    Serial.println(i);
    t = millis();
    if(newCounter++ == COUNT_THRESHH){
      newCounter=0;
      if((BinFull==false) & ((weight[0]/10 > WEIGHT_THRESH)|(level[0]>LEVEL_THRESH))){
        digitalWrite(EXT_LED, HIGH);
        BinFull = true;
        sendAlarm = true;
      }else if((BinFull==true) & ((weight[0]/10 < WEIGHT_THRESH)&(level[0]<LEVEL_THRESH))){
        digitalWrite(EXT_LED, LOW);
        BinFull = false;
        sendAlarm = false;
      }
    }
    if(BinFull)
        digitalWrite(EXT_LED, (!digitalRead(EXT_LED)));
  }
  buttonState = digitalRead(BUTTONPIN);
  if (buttonState != lastButtonState) { // Set tare to zero on using onboard Switch
    if (buttonState == LOW) {
      digitalWrite(LED, HIGH);
      LoadCell.tareNoDelay();     // reset the scale to 0
      delay(1000);
      digitalWrite(LED, LOW);
    }
  }
  lastButtonState = buttonState;
  if (Serial.available() > 0) {    //receive from serial terminal - to calibrate the scale
    float i;
    char inByte = Serial.read();
    if (inByte == 'l') i = -1.0;
    else if (inByte == 'L') i = -10.0;
    else if (inByte == 'h') i = 1.0;
    else if (inByte == 'H') i = 10.0;
    else if (inByte == 'm') i = 0.1;
    else if (inByte == 'M') i = -0.1;
    else if (inByte == 't') LoadCell.tareNoDelay();
    else if (inByte == 'p') {
      float v = LoadCell.getCalFactor();
      Serial.print("    Load_cell calFactor: ");
      Serial.println(v);
    }
    if (i != 't') {
      float v = LoadCell.getCalFactor() + i;
      LoadCell.setCalFactor(v);
    }
  }
  //check if last tare operation is complete
  if (LoadCell.getTareStatus() == true) {
    Serial.println("Tare complete");
  }
}
//===============================================================
// This routine sends SMS on Alert
//===============================================================
void TriggerAlerts(const char* API_Key, float Weight, float Level, const char* TrashbinName, bool isNativeTrigger) {
  if ((WiFi.status() == WL_CONNECTED)) {
    IFTTTWebhook wh(API_Key, IFTTT_EVENT_NAME); //Connect to IFTTT
    char WeightString[8], LevelString[8];
    Serial.println(dtostrf(Weight, 4, 2, WeightString));
    Serial.println(dtostrf(Level, 4, 2, LevelString));
    wh.trigger(WeightString, LevelString, TrashbinName);
    Serial.printf("Trigger has been broadcasted\n");
  }
}
//===============================================================
// This routine is to interface Ultrasonic Sensor
//===============================================================
void interfaceUSonic(void) {
  digitalWrite(TRIGPIN, LOW);         // Clears the TRIGPIN
  delayMicroseconds(2);
  digitalWrite(TRIGPIN, HIGH);        // Sets the TRIGPIN on HIGH state for 10 micro seconds
  delayMicroseconds(10);
  digitalWrite(TRIGPIN, LOW);
  duration = pulseIn(ECHOPIN, HIGH);  // Reads the ECHOPIN, returns the sound wave travel time in microseconds
  int distanc = duration * 0.034 / 2;    // Calculating the distance
  Serial.print("Distance (cm) : ");         // Prints the distance on the Serial Monitor
  Serial.print(distanc);
  Serial.print("  level (%) : ");         // Prints the distance on the Serial Monitor
  level[0] = rangePercentage(distanc);
  Serial.print(level[0]);
}
// Converts the Ultrasonic Output to Percentage of Trashbin filled
float rangePercentage(int input) {
  float percentage = (float)(((input - RANGE_MIN) * 100) / (RANGE_MAX - RANGE_MIN));
  if (percentage < 0)
    percentage = 0;
  return percentage;
}
//===============================================================
// This routine is to receive callback for the messages sent over Mesh node
//===============================================================
void receivedCallback( const uint32_t &from, const String &msg ) {
  Serial.printf("meshSketch: Received from %u msg=%s\n", from, msg.c_str());
  // Saving logServer
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(msg);
  if (root.containsKey("Packet")) {
    if (root["Packet"].as<int>() == PACKET_TYPE_0) { // check for type of packet
      weight[1] = root["LoadCell0"].as<float>();
      level[1] = root["UltrasonicSensor0"].as<float>();
      chipid[1] = root["chipsetID0"].as<unsigned long long>();
      Serial.printf("Handled from %u chipsetID0=%llu\n", from, chipid[1]);
      if (WiFi.status() == WL_CONNECTED) {
        myiot.WriteDashboard(root["USERID"], root["KEY"], (root["LoadCell0"].as<float>() / 10.0), root["UltrasonicSensor0"].as<float>(), 0.0, 0.0, root["NODENAME"], public_tweet);
        Serial.println("Sent tweet");
      }
    } else if (root["Packet"].as<int>() == PACKET_TYPE_1) { // Trigger Trash_Clean
      TriggerAlerts(root["IFTTTKey"], root["LoadCell0"].as<float>(), root["UltrasonicSensor0"].as<float>(), root["NODENAME"], false);
    }
  }
}
